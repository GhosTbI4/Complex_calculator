﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HALL_2;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void _0Multiplicate0returned0()
        {
            Complexnumber A = new Complexnumber(0, 0);
            Complexnumber B = new Complexnumber(0, 0);
            Complexnumber Expected = new Complexnumber(0, 0);
            A = A.Multiplication(A, B);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _10i0Multiplicate10i0returned100i0()
        {
            Complexnumber A = new Complexnumber(10, 0);
            Complexnumber B = new Complexnumber(10, 0);
            Complexnumber Expected = new Complexnumber(100, 0);
            A = A.Multiplication(A, B);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _negative10i10Multiplicate10i0returnedneg100i10()
        {
            Complexnumber A = new Complexnumber(-10, 0);
            Complexnumber B = new Complexnumber(10, 0);
            Complexnumber Expected = new Complexnumber(-100, 0);
            A = A.Multiplication(A, B);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _10i0Multiplicate0i0returned0i0()
        {
            Complexnumber A = new Complexnumber(0, 0);
            Complexnumber B = new Complexnumber(10, 0);
            Complexnumber Expected = new Complexnumber(0, 0);
            A = A.Multiplication(A, B);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _negative10i0Multiplicatenegative10i0returned100i0()
        {
            Complexnumber A = new Complexnumber(-10, 0);
            Complexnumber B = new Complexnumber(-10, 0);
            Complexnumber Expected = new Complexnumber(100, 0);
            A = A.Multiplication(A, B);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _negative10i0Divisionnegative10i0returned1i0()
        {
            Complexnumber A = new Complexnumber(-10, 0);
            Complexnumber B = new Complexnumber(-10, 0);
            Complexnumber Expected = new Complexnumber(1, 0);
            A = A.Division(A, B);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _10i0Division10i0returned1i0()
        {
            Complexnumber A = new Complexnumber(10, 0);
            Complexnumber B = new Complexnumber(10, 0);
            Complexnumber Expected = new Complexnumber(1, 0);
            A = A.Division(A, B);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _1_i1Division0returned1_i1i0()
        {
            Complexnumber A = new Complexnumber(1, 1);
            Complexnumber B = new Complexnumber(1, 1);
            Complexnumber Expected = new Complexnumber(1, 0);
            A = A.Division(A, B);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _10_i10Division10_i10returned1i0()
        {
            Complexnumber A = new Complexnumber(10, 10);
            Complexnumber B = new Complexnumber(10, 10);
            Complexnumber Expected = new Complexnumber(1, 0);
            A = A.Division(A, B);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
    }
}
