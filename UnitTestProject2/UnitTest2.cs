﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject2
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void _1_i_2_Csqrt8_5_returned_minus0_67_i_minus0_88()
        {
            Complexnumber A = new Complexnumber(1, 2);
            Complexnumber Expected = new Complexnumber(-0.67, -0.88);
            A = A.Csqrt(A, 8, 5);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _0_i_0_Csqrt10_4_returned_0_i_0()
        {
            Complexnumber A = new Complexnumber(0, 0);
            Complexnumber Expected = new Complexnumber(0, 0);
            A = A.Csqrt(A, 10, 4);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _33_i_0_Csqrt4_2_returned_munis2_4_i_0()
        {
            Complexnumber A = new Complexnumber(33, 0);
            Complexnumber Expected = new Complexnumber(-2.4, 0);
            A = A.Csqrt(A, 4, 2);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _0_i_9_Csqrt6_5_returned_1_02_i_minus1_02()
        {
            Complexnumber A = new Complexnumber(0, 9);
            Complexnumber Expected = new Complexnumber(1.02, -1.02);
            A = A.Csqrt(A, 6, 5);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _0_i_minus_21_Csqrt9_2_returned_minus0_48_i_1_32()
        {
            Complexnumber A = new Complexnumber(0, -21);
            Complexnumber Expected = new Complexnumber(-0.48, 1.32);
            A = A.Csqrt(A, 9, 2);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _minus12_i_0_Csqrt7_4_returned_minus0_89_i_minus1_12()
        {
            Complexnumber A = new Complexnumber(-12, 0);
            Complexnumber Expected = new Complexnumber(-0.89, -1.12);
            A = A.Csqrt(A, 7, 4);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _minus_5_i_minus_sqrt3_Csqrt5_1_returned_minus0_52_i_1_30()
        {
            Complexnumber A = new Complexnumber(-5, -Math.Sqrt(3));
            Complexnumber Expected = new Complexnumber(-0.52, 1.30);
            A = A.Csqrt(A, 5, 1);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _5_i_minus8_Csqrt4_1_returned_minus1_70_i_0_44()
        {
            Complexnumber A = new Complexnumber(5, -8);
            Complexnumber Expected = new Complexnumber(-1.70, 0.44);
            A = A.Csqrt(A, 4, 1);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _minus4_i_9_Csqrt4_2_returned_minus1_56_i_minus0_85()
        {
            Complexnumber A = new Complexnumber(-4, 9);
            Complexnumber Expected = new Complexnumber(-1.56, -0.85);
            A = A.Csqrt(A, 4, 2);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _4_50_i_minus9_20_Csqrt60_55_returned_0_94_i_minus0_44()
        {
            Complexnumber A = new Complexnumber(4.5, -9.2);
            Complexnumber Expected = new Complexnumber(0.94, -0.44);
            A = A.Csqrt(A, 60, 55);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }

        [TestMethod]
        public void _5_i_4_Cpow3_returned_minus115_i_236()
        {
            Complexnumber A = new Complexnumber(5, 4);
            Complexnumber Expected = new Complexnumber(-115, 236);
            A = A.Cpow(A, 3);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _minus9_i_3_Cpow2_returned_72_i_minus54()
        {
            Complexnumber A = new Complexnumber(-9, 3);
            Complexnumber Expected = new Complexnumber(72, -54);
            A = A.Cpow(A, 2);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _1_i_minus6_Cpow5_returned_6121_i_minus5646()
        {
            Complexnumber A = new Complexnumber(1, -6);
            Complexnumber Expected = new Complexnumber(6121, -5646);
            A = A.Cpow(A, 5);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _minus3_i_minus7_Cpow2_returned_6121_00_i_minus5646_00()
        {
            Complexnumber A = new Complexnumber(-3, -7);
            Complexnumber Expected = new Complexnumber(-40, 42);
            A = A.Cpow(A, 2);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _0_i_3_Cpow3_returned_0_i_minus27()
        {
            Complexnumber A = new Complexnumber(0, 3);
            Complexnumber Expected = new Complexnumber(0, -27);
            A = A.Cpow(A, 3);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _0_i_minus3_Cpow3_returned_0_i_27()
        {
            Complexnumber A = new Complexnumber(0, -3);
            Complexnumber Expected = new Complexnumber(0, 27);
            A = A.Cpow(A, 3);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _5_i_0_Cpow3_returned_125_i_0()
        {
            Complexnumber A = new Complexnumber(5, 0);
            Complexnumber Expected = new Complexnumber(125, 0);
            A = A.Cpow(A, 3);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _minus5_i_0_Cpow3_returned_minus125_i_0()
        {
            Complexnumber A = new Complexnumber(-5, 0);
            Complexnumber Expected = new Complexnumber(-125, 0);
            A = A.Cpow(A, 3);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _0_i_0_Cpow10_returned_0_i_0()
        {
            Complexnumber A = new Complexnumber(0, 0);
            Complexnumber Expected = new Complexnumber(0, 0);
            A = A.Cpow(A, 10);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _0_i_0_Cpow0_returned_1_i_0()
        {
            Complexnumber A = new Complexnumber(0, 0);
            Complexnumber Expected = new Complexnumber(1, 0);
            A = A.Cpow(A, 0);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _1_i_minus6_Cpow0_returned_1_i_0()
        {
            Complexnumber A = new Complexnumber(1, -6);
            Complexnumber Expected = new Complexnumber(1, 0);
            A = A.Cpow(A, 0);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _minus3_i_minus7_Cpow_minus1_returned_minus0_05_i_0_12()
        {
            Complexnumber A = new Complexnumber(-3, -7);
            Complexnumber Expected = new Complexnumber(-0.05, 0.12);
            A = A.Cpow(A, -1);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }
        [TestMethod]
        public void _3_4_i_2_3_Cpow10_returned_1282538_9_i_minus447331_38()
        {
            Complexnumber A = new Complexnumber(3.4, 2.3);
            Complexnumber Expected = new Complexnumber(1282538.9, -447331.38);
            A = A.Cpow(A, 10);
            Assert.AreEqual(A.getreal(), Expected.getreal());
            Assert.AreEqual(A.getimaginary(), Expected.getimaginary());
        }

        [TestMethod]
        public void _0_i_0_Arg_returned_0_()
        {
            Complexnumber A = new Complexnumber(0, 0);
            double Expected = 0;
            Assert.AreEqual(A.Arg(A), Expected);
        }
        [TestMethod]
        public void _1_i_0_Arg_returned_0_()
        {
            Complexnumber A = new Complexnumber(1, 0);
            double Expected = 0;
            Assert.AreEqual(A.Arg(A), Expected);
        }
        [TestMethod]
        public void _1_i_1_Arg_returned_PI_4()
        {
            Complexnumber A = new Complexnumber(1, 1);
            double Expected = Math.PI / 4;
            Assert.AreEqual(A.Arg(A), Expected);
        }
        [TestMethod]
        public void _0_i_1_Arg_returned_PI_2()
        {
            Complexnumber A = new Complexnumber(0, 1);
            double Expected = Math.PI / 2;
            Assert.AreEqual(A.Arg(A), Expected);
        }
        [TestMethod]
        public void _minus1_i_1_Arg_returned_3PI_4()
        {
            Complexnumber A = new Complexnumber(-1, 1);
            double Expected = 3 * Math.PI / 4;
            Assert.AreEqual(A.Arg(A), Expected);
        }
        [TestMethod]
        public void _minus1_i_0_Arg_returned_PI()
        {
            Complexnumber A = new Complexnumber(-1, 0);
            double Expected = Math.PI;
            Assert.AreEqual(A.Arg(A), Expected);
        }
        [TestMethod]
        public void _minus1_i_minus1_Arg_returned_5PI_4()
        {
            Complexnumber A = new Complexnumber(-1, -1);
            double Expected = 5 * Math.PI / 4;
            Assert.AreEqual(A.Arg(A), Expected);
        }
        [TestMethod]
        public void _0_i_minus1_Arg_returned_3PI_2()
        {
            Complexnumber A = new Complexnumber(0, -1);
            double Expected = 3 * Math.PI / 2;
            Assert.AreEqual(A.Arg(A), Expected);
        }
        [TestMethod]
        public void _1_i_minus1_Arg_returned_7PI_4()
        {
            Complexnumber A = new Complexnumber(1, -1);
            double Expected = 7 * Math.PI / 4;
            Assert.AreEqual(A.Arg(A), Expected);
        }

    }
}
