﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MathNet;
using System.Numerics;
namespace HALL_2
{
    public partial class Form1 : Form
    {

        Complexnumber A, B;

        public Form1()
        {

            InitializeComponent();
            comboBox1.Items.Add("число А");
            comboBox1.Items.Add("число B");

            comboBox3.Items.Add("число А");
            comboBox3.Items.Add("число B");

            comboBox2.Items.Add("+");
            comboBox2.Items.Add("-");
            comboBox2.Items.Add("/");
            comboBox2.Items.Add("*");
            comboBox2.Items.Add("=");
            comboBox2.Items.Add("^");
            comboBox2.Items.Add("√");
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.Text == "√" || comboBox2.Text == "^")
            {
                label2.Text = "N";
                textBox4.Enabled = false;
            }
            else
            {
                label2.Text = "число B";
                textBox4.Enabled = true;
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox5.Text = "";
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            double newreal1, newimaginary1, newreal2, newimaginary2;
            if (double.TryParse(textBox1.Text, out newreal1) && double.TryParse(textBox3.Text, out newimaginary1) && double.TryParse(textBox2.Text, out newreal2) && double.TryParse(textBox4.Text, out newimaginary2))
            {
                A = new Complexnumber(newreal1, newimaginary1);
                B = new Complexnumber(newreal2, newimaginary2);

                textBox5.Clear();
                if (comboBox2.SelectedItem.ToString() == "*")
                {
                    A = A.Multiplication(A, B);
                    textBox5.Text += A.getreal() + " ";
                    textBox5.Text += A.getimaginary();
                }
                else if (comboBox2.SelectedItem.ToString() == "/")
                {
                    A = A.Division(A, B);
                    textBox5.Text += A.getreal() + " ";
                    textBox5.Text += A.getimaginary();
                }
                else if (comboBox2.SelectedItem.ToString() == "+")
                {
                    A = A.Sum(A, B);
                    textBox5.Text += A.getreal() + " ";
                    textBox5.Text += A.getimaginary();
                }
                else if (comboBox2.SelectedItem.ToString() == "-")
                {
                    A = A.Sub(A, B);
                    textBox5.Text += A.getreal() + " ";
                    textBox5.Text += A.getimaginary();
                }
                else if (comboBox2.SelectedItem.ToString() == "=")
                {
                    bool b = A.Equal(A, B);
                    if (b == true)
                    {
                        textBox5.Text += "Числа равны";
                    }
                    else
                    {
                        textBox5.Text += "Числа не равны";
                    }
                }
                else if ((string)comboBox2.SelectedItem == "^")
                {
                    Complexnumber C = A.Cpow(A, Convert.ToInt32(textBox2.Text));
                    textBox5.Text += C.getreal().ToString() + ' ' + C.getimaginary().ToString() + Environment.NewLine;
                }
                else if ((string)comboBox2.SelectedItem == "√")
                {
                    textBox5.Clear();
                    int k = Convert.ToInt32(textBox2.Text);
                    Complexnumber C;
                    for (int i = 0; i < k; i++)
                    {
                        C = A.Csqrt(A, k, i);
                        textBox5.Text += C.getreal().ToString() + ' ' + C.getimaginary().ToString() + Environment.NewLine;
                    }
                }
            }
        }
    }
}

public class Complexnumber
{
    public Complexnumber(double newreal, double newimaginary)
    {
        real = newreal;
        imaginary = newimaginary;
    }
    double real, imaginary;
    public double getreal()
    {
        return real;
    }
    public double getimaginary()
    {
        return imaginary;
    }
    public void setreal(double newreal)
    {
        real = newreal;
    }
    public void setimaginary(double newimaginary)
    {
        imaginary = newimaginary;
    }

    public Complexnumber Multiplication(Complexnumber A, Complexnumber B)
    {
        Complexnumber C = new Complexnumber(0, 0);
        if (A != null && B != null)
        {
            C.real = A.real * B.real + (-1) * A.imaginary * B.imaginary;
            C.imaginary = A.real * B.imaginary + A.imaginary * B.real;
        }
        return C;

    }

    public Complexnumber Division(Complexnumber A, Complexnumber B)
    {
        Complexnumber C = new Complexnumber(0, 0);
        if (A != null && B != null)
        {
            C.real = (A.real * B.real + A.imaginary * B.imaginary) / (B.real * B.real + B.imaginary * B.imaginary);
            C.imaginary = (B.real * A.imaginary + (-1) * B.imaginary * A.real) / (B.real * B.real + B.imaginary * B.imaginary);
        }
        return C;

    }

    public Complexnumber Sum(Complexnumber A, Complexnumber B)
    {
        Complexnumber C = new Complexnumber(0, 0);
        if (A != null && B != null)
        {
            C.real = A.real + B.real;
            C.imaginary = A.imaginary + B.imaginary;
        }
        return C;
    }

    public Complexnumber Sub(Complexnumber A, Complexnumber B)
    {
        Complexnumber C = new Complexnumber(0, 0);
        if (A != null && B != null)
        {
            C.real = A.real - B.real;
            C.imaginary = A.imaginary - B.imaginary;
        }
        return C;
    }

    public bool Equal(Complexnumber A, Complexnumber B)
    {
        if (A != null && B != null)
        {
            if (A.real == B.real && A.imaginary == B.imaginary)
            {
                return true;
            }
        }
        return false;
    }
    public Complexnumber Cpow(Complexnumber z, int n)
    {
        double x, y;
        double r = Math.Sqrt(z.getreal() * z.getreal() + z.getimaginary() * z.getimaginary());

        x = Math.Round(Math.Pow(r, n) * Math.Cos(n * Arg(z)), 2);
        y = Math.Round(Math.Pow(r, n) * Math.Sin(n * Arg(z)), 2);
        z = new Complexnumber(x, y);
        return z;
    }

    public Complexnumber Csqrt(Complexnumber z, int n, int k)
    {
        double x, y;
        double r = Math.Sqrt(z.getreal() * z.getreal() + z.getimaginary() * z.getimaginary());

        x = Math.Round(Math.Pow(r, 1.0 / n) * Math.Cos((Arg(z) + 2 * k * Math.PI) / n), 2);
        y = Math.Round(Math.Pow(r, 1.0 / n) * Math.Sin((Arg(z) + 2 * k * Math.PI) / n), 2);
        z = new Complexnumber(x, y);
        return z;
    }

    public double Arg(Complexnumber z)
    {
        double fi;
        if (z.getreal() > 0 && z.getimaginary() < 0) fi = 2 * Math.PI + Math.Atan(z.getimaginary() / z.getreal());
        else if (z.getreal() > 0 && z.getimaginary() >= 0) fi = Math.Atan(z.getimaginary() / z.getreal());
        else if (z.getreal() < 0 && z.getimaginary() != 0) fi = Math.Atan(z.getimaginary() / z.getreal()) + Math.PI;
        else if (z.getreal() < 0 && z.getimaginary() == 0) fi = Math.PI;
        else if (z.getreal() == 0 && z.imaginary > 0) fi = Math.PI / 2;
        else if (z.getreal() == 0 && z.imaginary < 0) fi = 3 * Math.PI / 2;
        else fi = 0;
        return fi;
    }
}